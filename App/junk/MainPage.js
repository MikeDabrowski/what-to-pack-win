import React, { Component } from 'react';
import {
  FlatList,
  StyleSheet,
  Text, TouchableOpacity,
  View
} from 'react-native';

import { tripDestinations } from '../data/data';
import { weatherConditions } from '../data/data';
import { tripActivities } from '../data/data';

export default class MainPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {

    return (
      <View flex={1} justifyContent="center" alignItems="center">

        <Text>Where do you want to go?</Text>
        {/*<FlatList data={Object.values(tripDestinations).map( el => { el.key=el.name; return el})}
          renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}/>

        <Text>How do you get there?</Text>
        <Text>Are you traveling alone?</Text>
        <Text>For how long?</Text>
        <Text>Accommodation</Text>

        <Text>Expected weather conditions</Text>
        <FlatList data={Object.values(weatherConditions).map( el => { el.key=el.name; return el})}
          renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}/>

        <Text>Trip type</Text>
        <FlatList data={Object.values(tripTypes).map( el => { el.key=el.name; return el})}
          renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}/>

        <TouchableOpacity onPress={this.navigateToNextScreen.bind(this)}>
          <Text>Go to list</Text>
        </TouchableOpacity>*/}
      </View>
    );
  }

  navigateToNextScreen() {
    this.props.navigation.navigate("PackingList")
    // notice that the "SecondScreen" is the name of the component
    // Screen_1 in the AppStack
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

