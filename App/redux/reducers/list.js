import * as types from '../actions/actionTypes';

export default function list(state = {}, action) {
  switch (action.type) {
    case types.SET_LIST:
      return action.list;
    case types.CLEAR_LIST:
      return null;
    default:
      return state
  }
}