import { combineReducers } from 'redux'
import * as types from '../actions/actionTypes';
import list from './list';
import listItems from './listItems';
import canGenList from './canGenList';
import textOptions from './textOptions';

const appReducer = combineReducers({
  listItems,
  canGenList,
  textOptions,
  list
});

export default WhatToPack = (state, action) => {
  if(action.type === types.RESET_APP){
    state = undefined;
  }
  return appReducer(state, action)
}
