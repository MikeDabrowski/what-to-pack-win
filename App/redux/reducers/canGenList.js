import * as types from '../actions/actionTypes';

export default canGenList = (state = false, action) => {
  switch (action.type) {
    case types.CAN_GEN_LIST:
      return true;
    case types.CANT_GEN_LIST:
      return false;
    default:
      return state
  }
};
