import React, {Component} from 'react';
import { Button, Text, TextInput, View } from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as Actions from '../../redux/actions/actions'


class NumberInputComponent extends Component {
  showState() {
    console.log('showing state', this.props.store);
  }

  /*handlePress = (item) => {
    const listItems = this.props.store.default.listItems;
    const listName = this.props.data.name;
    const index = listItems[listName] && listItems[listName].findIndex((el) => el === item);
    if (index > -1) {
      this.props.actions.removeItemFromList(listName, item);
    } else {
      this.props.actions.addItemToList(listName, item);
    }
  };*/

  onChange = (text) => {
    this.props.actions.optionUpdate('first', text);
    console.log('numInput onChange', this.props.store.default.textOptions);
  };

  render() {
    return (
      <View flex={1} style={{alignItems: 'center'}}>
        <Text>Simple element</Text>
        <Button title="Show state" onPress={() => this.showState()} color='red'/>
        <TextInput
          onChangeText={(text) => this.onChange(text)}
          value={this.props.store.default.textOptions.first}
        />
        <Button title="Show state" onPress={() => this.showState()} color='red'/>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NumberInputComponent);
