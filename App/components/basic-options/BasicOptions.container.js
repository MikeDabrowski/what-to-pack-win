import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../redux/actions/actions'
import BasicOptions from './BasicOptions';

class BasicOptionsComponent extends Component {
  handleInput = (option, input) => {
    this.props.actions.optionUpdate(option, input);
  };

  isValid() {
    const to = this.props.store.default.textOptions;
    return this.props.submitted ? to.date && to.days : true;
  }

  render() {
    return (
      <View flex={1} style={[styles.basic, this.isValid() ? null : styles.invalid]}>
        <BasicOptions submitted={this.props.submitted} handleInput={(option, value) => {
          this.handleInput(option, value)
        }} data={this.props.store.default.textOptions}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  basic: {
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  header: {
    fontSize: 25
  },
  invalid: {
    paddingRight: 20,
    paddingLeft: 20
  }
});

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BasicOptionsComponent);
