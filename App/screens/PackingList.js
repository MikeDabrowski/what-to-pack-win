import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../redux/actions/actions';

class PackingList extends Component {
  handlePress = (item) => {
    item.packed = !item.packed;
    this.forceUpdate();
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.data = [];
  }

  prepareData() {
    return Object.entries(this.props.store.default.list).map(el=>({
      title: el[0],
      data: el[1].map(i => {
        if(i['packed'] === undefined) i['packed'] = false;
        return i;
      })
    }));
  }

  renderCheckBox(rowItem) {
    if (rowItem.packed) {
      return (<IconMCI size={30} name={'checkbox-marked-outline'}/>)
    } else {
      return (<IconMCI size={30} name={'checkbox-blank-outline'}/>)
    }
  }

  renderRow(rowItem, rowKey) {
    return (
      <TouchableOpacity style={styles.row}
                        key={rowKey}
                        onPress={() => this.handlePress(rowItem)}>
        {this.renderCheckBox(rowItem)}
        <Text style={[styles.item,{flex: 1}]}>{rowItem.name}</Text>
        <Text style={[styles.item,{alignSelf: 'flex-end'}]}>{rowItem.qty}</Text>
      </TouchableOpacity>
    )
  }

  renderCategory(category, categoryKey) {
    return (
      <View key={categoryKey}>
        <Text style={styles.categoryHeader}>{category.title}</Text>
        {category.data.map((rowItem, rowKey) => {
          return (this.renderRow(rowItem, rowKey))
        })}
      </View>
    )
  }

  renderList() {
    let data = this.prepareData();
    return (
      data.map((category, categoryKey) => {
        return (this.renderCategory(category, categoryKey))
      })
    )
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.renderList()}
      </ScrollView>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {store: state}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PackingList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  categoryHeader: {
    fontSize: 20,
    paddingLeft: 10,
    fontWeight: 'bold',
    backgroundColor: 'rgba(240,240,240,1.0)',
    elevation: 2
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center'
  }
});

